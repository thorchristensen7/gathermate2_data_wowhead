generate:
	python3 generate_data.py && \
	sed -i "s/## Version: v.*/## Version: v$$(date +%F)/" GatherMate2_Data/GatherMate2_Data.toc && \
	awk -F'"' '/GatherMateData.generatedVersion/ {$$2=$$2+1}1' OFS='"' GatherMate2_Data/GatherMateData.lua > tmp && mv tmp GatherMate2_Data/GatherMateData.lua

commit:
	git commit -a -m "Updates for $$(grep '## Version' GatherMate2_Data/GatherMate2_Data.toc | cut -d'v' -f2)"

zip:
	zip -r gathermate2_data_wowhead_classic_v$$(grep "## Version" GatherMate2_Data/GatherMate2_Data.toc | cut -d"v" -f2).zip GatherMate2_Data/

update:
	$(MAKE) generate && \
	$(MAKE) commit && \
	$(MAKE) zip

clean:
	git clean -ffxd -e .idea
